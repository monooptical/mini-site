<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>NEW! Groundbreaking ENERGY FOR PERFORMANCE® training solution</title>
		<link rel="stylesheet" type="text/css" href="css/all.css" media="screen"/>
		
		<script type="text/javascript" src="/js/jquery-1.8.0.js"></script>
		<script type="text/javascript" src="/js/jquery.main.js"></script>
		<script type="text/javascript">
			var youtubePlayer;
			function onYouTubePlayerReady(ytplayer) {
				console.log("YouTube player is ready");
				console.log(youtubePlayer);
				youtubePlayer = document.getElementById("youtube-player");
				if (youtubePlayer == undefined) {
					youtubePlayer = document.getElementById("youtube-player-ie");
				}
			}
			$(document).ready(function() {
				$('a.play').click(function() {
					youtubePlayer.playVideo();
					return false;
				});
			});
		</script>
	</head>
	<body>
		<div id="wrapper">
			<div id="header-wrapper">
				<div id="header" class="w1">
					<strong class="logo"><a href="/">human performance institute</a></strong>
					<strong class="slogan">Energy for performance e-course</strong>
					<div class="contact">
						<p>Call <span>1-407-438-9911</span><br /> or <a href="mailto:info@hpinstitute.com">Contact Us</a> via Email</p>
					</div>
				</div>
			</div>
			<div id="main-wrapper">
				<div id="main" class="w1">
					<div class="banner-block">
						<div class="text-area">
							<h1>Your Entire Organization Can Learn the Secrets of Elite Performers!</h1>
							<p>For over 30 years, Fortune 500 organizations have trusted the Human Performance Institute’s energy management training solutions to help improve performance levels of their workforce, enhance overall engagement, and support health and wellness initiatives. They have come to understand that human energy is the most critical resource they have.<br/><br/>

For the first time ever, this energy management training, previously available primarily to senior leaders, is available to everyone in your organization. The ENERGY FOR PERFORMANCE®E-COURSE is an engaging, scalable, video-rich online course that helps empower participants to create lifelong behavior change to help bring their highest potential to their work and life.</p>
						</div>
						<div class="placeholder-holder">
							<div class="placeholder-video">
								<div class="text-holder">
									<p>Learn how the ENERGY FOR PERFORMANCE® e-Course<br/>can impact your organization in this 3-minute video.</p>
								</div>
								<img src="images/img01.jpg" alt="image description" width="670" height="372" />
								<a href="#" class="play">play</a>
							</div>
							<div id="video-player">
								<object width="681" height="383" id="youtube-player-ie"><param name="movie" value="http://www.youtube.com/v/laO69PtqP0o?version=3&amp;hl=en_US&amp;enablejsapi=1&amp;playerapiid=youtubePlayer&amp;showinfo=0&amp;rel=0"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/laO69PtqP0o?version=3&amp;hl=en_US&amp;enablejsapi=1&amp;playerapiid=youtubePlayer&amp;showinfo=0&amp;rel=0" type="application/x-shockwave-flash" width="681" height="383" allowscriptaccess="always" allowfullscreen="true" id="youtube-player"></embed></object>
							</div>
						</div>
					</div>
					<div class="section">
						<div class="program-block">
							<div class="heading">
								<h2>Program Overview</h2>
								<h3>Downloadable PDF Files</h3>
							</div>
							<div class="item-list">
								<div>
									<div class="title"><h3 class="course"><a href="/course-contents.php">Course Contents</a></h3></div>
									<p>The course includes 6 topics containing a total of 17 lessons with application exercises to further enhance the learning experience.</p>
									<a href="/course-contents.php" class="more">Read More</a>
								</div>
								<div>
									<div class="title"><h3 class="about"><a href="/about.php">About Us</a></h3></div>
									<p>The Human Performance Institute’s technology of managing energy is measurement-based and grounded in the sciences of performance psychology, exercise physiology, and nutrition.</p>
									<a href="/about.php" class="more">Read More</a>
								</div>
								<div>
									<div class="title"><h3 class="impl"><a href="/overview.php">Turnkey<br />Implementation</a></h3></div>
									<p>Our Implementation Team can work with you to develop an Implementation Strategy, share best practices, and design a Rollout plan that works for your organization.</p>
									<a href="/overview.php" class="more">Read More</a>
								</div>
							</div>
							<div class="download-block">
								<h3>Downloads</h3>
								<p>Click to download this course overview. It provides specifics about the ENERGY FOR PERFORMANCE® e-Course and outlines the benefits for both participants and the organization.</p>
								<div class="btn-down">
									<a href="Energy for Performance eCourse flyer.pdf">Energy for Performance eCourse flyer</a>
									<span>: (PDF, 42 KB)</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="footer-wrapper">
				<div id="footer" class="w1">
					<ul class="nav">
						<li><a href="/about.php">E-Course Overview </a></li>
						<li><a href="/course-contents.php">E-Course Benefits</a></li>
						<li><a href="/overview.php#">Roll-Out Tools</a></li>
						<li><a href="http://www.hpinstitute.com/">hpinstitute.com</a></li>
						<li><a href="http://get.adobe.com/reader/" target="_blank">Get Adobe PDF Reader</a></li>
					</ul>
					<div class="info">
						<address>9757 Lake Nona Road Orlando, Florida 32827</address>
						<dl>
							<dt>Phone:</dt>
							<dd>407.438.9911</dd>
							<dt>Fax:</dt>
							<dd>407.438.6667</dd>
						</dl>
					</div>
					<ul class="social-icon">
						<li><a href="http://www.linkedin.com/company/human-performance-institute" class="linkedin" target="_blank">linkedin</a></li>
						<li><a href="http://www.facebook.com/HumanPerformanceInstitute" class="facebook" target="_blank">facebook</a></li>
						<li><a href="http://www.twitter.com/teamhpi" class="twitter" target="_blank">twitter</a></li>
						<li><a href="http://www.youtube.com/watch?v=Gxq7sQoWxyg&playnext=1&list=PL0BE45982D331DC20&feature=results_main" class="youtube" target="_blank">youtube</a></li>
					</ul>
					<p>This site is governed solely by applicable U.S. laws and governmental regulations. Please see our Privacy Policy. Use of this site constitutes your consent to application of such laws and regulations and to our Privacy Policy. Your use of the information on this site is subject to the terms of our Legal Notice. You should view the News section and the most recent SEC Filings in the Investor section in order to receive the most current information made available by Johnson &amp; Johnson Services, Inc. Contact Us with any questions or search this site for more information.</p>
					<p class="copy">All contents Copyright &copy; 2009-2013 Human Performance Institute, Inc. All Rights Reserved.</p>
				</div>
			</div>
		</div>
	</body>
</html>

