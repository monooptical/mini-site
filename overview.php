<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Change the title of this page to: Best Practice Implementation Tools</title>
	<link rel="stylesheet" type="text/css" href="css/slide.css" media="all"/>
	<script type="text/javascript" src="js/jquery-1.8.3.min.js" ></script>
	<script type="text/javascript" src="js/jquery.main.js" ></script>
</head>
<body>
	<noscript><div>javascript must be enabled for the correct page display</div></noscript>
	<div id="wrapper">
		<div class="w1">
			<div id="header">
				<div class="holder">
					<a href="#main" class="skip" tabindex="1">Skip To Content</a>
					<strong class="logo"><a href="/" tabindex="1">Human perfomance insyitute</a></strong>
					<h1>Energy for performance e-course</h1>
					<div class="contact">
						<p>Call <span>1-407-438-9911</span><br /> or <a href="mailto:info@hpinstitute.com">Contact Us</a> via Email</p>
					</div>
				</div>
			</div>
			<div id="main">
				<div class="gallery-holder">
					<div class="gallery-frame">
						<div class="slideshow">
							<div class="slideset">
								<div class="slide">
									<div class="col-holder">
										<div class="col">
											<div class="heading">
												<img class="alignleft" src="images/icon-keynote.png" alt="icon-keynote" width="99" height="100" />
												<h2>Live Keynote</h2>
												<div class="row">
													
												</div>
											</div>
											<p>For any organization, a live keynote presentation conducted by one of our Performance Coaches is the ideal way to kick-off an ENERGY FOR PERFORMANCE® e-Course roll-out.  These motivational, in-person meetings allow for a taste of the e-Course and can leave attendees excited about participating.</p>
										</div>
										<div class="col">
											<div class="heading">
												<img class="alignleft" src="images/icon-email.png" alt="image description" width="99" height="100" />
												<div class="text-holder">
													<h2>Participant Invitation</h2>
													<div class="row">
														
													</div>
												</div>
											</div>
											<p>This sample email content can be used to set context for the e-Course participants and invite them to a kick-off live/webinar meeting where they can learn more about the e-Course and how to participate.</p>
										</div>
									</div>
									<div class="gallery-title">
										<h3>Best Practice Tools</h3>
									</div>
								</div>
								<div class="slide">
									<div class="col-holder">
										<div class="col">
											<div class="heading">
												<img class="alignleft" src="images/icon-awards.png" alt="image description" width="99" height="100" />
												<h2>Awareness Campaign</h2>
												<div class="row">
													
												</div>
											</div>
											<p>Based on consumer insights and leveraging unique Energy Management “A-ha’s”, these versatile marketing templates help create awareness and will help drive enrollment in the e-Course.  Additionally, the Energy for Performance promotional video can be used to pique interest.  This 3-minute <a href="http://youtu.be/laO69PtqP0o" target="_blank">video</a> showcases the power of the e-Course and helps drive demand for the e-Course.</p>
										</div>
										<div class="col">
											<div class="heading">
												<img class="alignleft" src="images/icon-kickoff.png" alt="image description" width="99" height="100" />
												<div class="text-holder">
													<h2>Kick-off Meeting</h2>
													<div class="row">
														
													</div>
												</div>
											</div>
											<p>This template for a live/web meeting can be leveraged to set context and gain participant commitment.  The kick-off meeting provides a forum to communicate the “What’s in it for me” benefits, set timelines,  and explain how to access the e-Course. Ideally, a senior sponsor will emphasize the importance of the course in-person or on video.</p>
										</div>
									</div>
									<div class="gallery-title">
										<h3>Best Practice Tools</h3>
									</div>
								</div>
								<div class="slide">
									<div class="col-holder">
										<div class="col">
											<div class="heading">
												<img class="alignleft" src="images/icon-email.png" alt="image description" width="99" height="100" />
												<h2>Manager as Coach Email</h2>
												<div class="row">
													
												</div>
											</div>
											<p>In addition to senior management support, participants are more likely to fully engage with the e-Course if they also have their direct manager’s support.  This email not only provides sample content to encourage leaders to support their direct reports to participate but also how they can help enhance the experience via engaging questions and simple changes to the work environment.</p>
										</div>
										<div class="col">
											<div class="heading">
												<img class="alignleft" src="images/icon-kickoff.png" alt="image description" width="99" height="100" />
												<div class="text-holder">
													<h2>e-Course</h2>
													<div class="row">
														
													</div>
												</div>
											</div>
											<p>The Energy for Performance® e-Course is an engaging, video-rich online course that helps you learn how to effectively manage and expand the energy you have available to meet life’s increasing demands.  Based on the sciences of performance psychology, exercise physiology and nutrition, this 4 hour course equips you with the knowledge and tools you need to be able to perform in today’s high-stress environments without sacrificing what matters most to you.</p>
										</div>
									</div>
									<div class="gallery-title">
										<h3>Best Practice Tools</h3>
									</div>
								</div>
								<div class="slide">
									<div class="col-holder">
										<div class="col">
											<div class="heading">
												<img class="alignleft" src="images/icon-follow.png" alt="image description" width="99" height="100" />
												<h2>Follow-up Sharing Session</h2>
												<div class="row">
													
												</div>
											</div>
											<p>A post-course sharing session is considered a key best practice and a forum to help solidify accountability and individual change. Not only can this session help strengthen the participant’s commitment to the Energy for Performance principles, but the “buzz” generated by this peer-to-peer platform will help accelerate organizational adoption. This is also a great vehicle for capturing success stories to share throughout the organization.</p>
										</div>
										<div class="col">
											<div class="heading">
												<img class="alignleft" src="images/icon-kickoff.png" alt="image description" width="99" height="100" />
												<div class="text-holder">
													<h2>Sustainability Webinar</h2>
													<div class="row">
														
													</div>
												</div>
											</div>
											<p>We believe that a best-in-class implementation program is not complete without sustainability strategies to help reinforce and maintain behavior change. These best practice sustainability tips will help you develop a strategy that works in your organization’s environment. Additionally, we’ve created a turn-key toolkit of content and templates that bring these best practices to life (sustainability toolkit available for an additional fee).</p>
										</div>
									</div>
									<div class="gallery-title">
										<h3>Best Practice Tools</h3>
									</div>
								</div>
								<div class="slide">
									<div class="col-holder">
										<div class="col">
											<div class="heading">
												<img class="alignleft" src="images/icon-story.png" alt="image description" width="99" height="100" />
												<h2>Success Stories</h2>
												<div class="row">
													
												</div>
											</div>
											<p>Create a wave of excitement for the ENERGY FOR PERFORMANCE e-COURSE with participant success stories. Start with input from your CEO! Capture success stories with video and post to your portal or share via email. Tie “story sharing” to your organization’s reward program. Smartphone video will do!</p>
										</div>
										<div class="col">
											<div class="heading">
												<img class="alignleft" src="images/icon-kickoff.png" alt="image description" width="99" height="100" />
												<div class="text-holder">
													<h2>Your portal </h2>
													<div class="row">
														
													</div>
												</div>
											</div>
											<p>Create a program page on your organization’s intranet.  Showcase success stories, post the <a href="http://youtu.be/laO69PtqP0o"target="_blank"> e-Course Promotional Video</a> and your organization’s Executive Sponsor video. Periodically refresh the content with ongoing video testimonials. </p>
										</div>
									</div>
									<div class="gallery-title">
										<h3>COMMUNICATION PLATFORMS</h3>
									</div>
								</div>
								<div class="slide">
									<div class="col-holder">
										<div class="col">
											<div class="heading">
												<img class="alignleft" src="images/icon-follow.png" alt="image description" width="99" height="100" />
												<h2>Social Tools </h2>
												<div class="row">
													
												</div>
											</div>
											<p>Leverage your organization’s social tools to connect participants and spread the word. Identify program champions to seed conversations (see our Sustainability Best Practices Tool for discussion topics) or solicit testimonials.</p>
										</div>
										<div class="col">
											<div class="heading">
												<img class="alignleft" src="images/icon-story.png" alt="image description" width="99" height="100" />
												<div class="text-holder">
													<h2>Executive Sponsor Message</h2>
													<div class="row">
														
													</div>
												</div>
											</div>
											<p>Leadership commitment is critical to participation. Leverage champions by capturing Executive Sponsors in brief video clips, and invite them to tell their story. Solicit responses to “What did you get out of this program personally? What can others expect to get out of it? What’s in it for our business?”Showcase the videos on your intranet or program portal. Reinforce sponsorship with an <strong>Executive Sponsor Email.</strong></p>
										</div>
									</div>
									<div class="gallery-title">
										<h3>COMMUNICATION PLATFORMS</h3>
									</div>
								</div>
								<div class="slide">
									<div class="col-holder">
										<div class="col">
											<div class="heading">
												<img class="alignleft" src="images/icon-eat.png" alt="image description" width="99" height="100" />
												<h2>Eat Light, Eat Often Mini Event</h2>
												<div class="row">
												
												</div>
											</div>
											<p>Conduct an ‘Eating Light, Eating Often’ event.  Solicit a champion to share snacking ideas and strategies.  Or hold a ’Cooking Light’ demo in your organization’s cafeteria, showcasing simple evening meal strategies. Create a follow-up forum for sharing fast, healthy dinner recipes. </p>
										</div>
										<div class="col">
											<div class="heading">
												<img class="alignleft" src="images/icon-recovery.png" alt="image description" width="99" height="100" />
												<div class="text-holder">
													<h2>Recovery Mini-Event</h2>
													<div class="row">
														
													</div>
												</div>
											</div>
											<p>Host a 5K walk or run. Invite internal champions to lead impromptu yoga, breathing or stretching mini-breaks.  Leverage your organization’s desktop TV or internal broadcast systems to get EVERYONE moving. </p>
										</div>
									</div>
									<div class="gallery-title">
										<h3>COMMUNICATION PLATFORMS</h3>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="pagination-holder">
						<div class="pagination-frame">
							<a class="btn-prev" href="#">Previous</a>
							<div class="pagination">
								<ul>
									<li class="first-child"><a href="#">1</a></li>
									<li><a href="#">2</a></li>
									<li><a href="#">3</a></li>
									<li class="last-child"><a href="#">4</a></li>
									<li class="alt first-child"><a href="#">5</a></li>
									<li class="alt"><a href="#">6</a></li>
									<li class="alt last-child"><a href="#">7</a></li>
								</ul>
							</div>
							<a class="btn-next" href="#">Next</a>
						</div>
					</div>
				</div>
				<div class="slide-block active">
					<div class="slide-holder">
						<div class="slide">
							<h2>Best Practice Tools</h2>
							<ul class="arrow-list">
								<li>
									<a href="#">
										<span>
											<span class="img-holder first">
												<img src="images/img1.png" width="52" height="58" alt="image description" />
											</span>
											<span class="text first"><em>Live Keynote</em> Event</span>
										</span>
										<span>
											<span class="img-holder">
												<img src="images/img2.png" width="57" height="38" alt="image description" />
											</span>
											<span class="text"><em>Participation</em> Invitation</span>
										</span>
									</a>
								</li>
								<li>
									<a href="#">
										<span>
											<span class="img-holder">
												<img src="images/img3.png" width="47" height="49" alt="image description" />
											</span>
											<span class="text"><em>Awareness</em> Campaign</span>
										</span>
										<span class="mark">
											<span class="img-holder">
												<img src="images/img4.png" width="48" height="45" alt="image description" />
											</span>
											<span class="text"><em>Kickoff</em>Meeting</span>
										</span>
									</a>
								</li>
								<li>
									<a href="#">
										<span>
											<span class="img-holder">
												<img src="images/img2.png" width="57" height="38" alt="image description" />
											</span>
											<span class="text"><em>Manager</em> as Coach</span>
										</span>
										<span>
											<span class="img-holder">
												<img src="images/img4.png" width="48" height="45" alt="image description" />
											</span>
											<span class="text">E-Course</span>
										</span>
									</a>
								</li>
								<li>
									<a href="#">
										<span class="mark">
											<span class="img-holder">
												<img src="images/img5.png" width="44" height="45" alt="image description" />
											</span>
											<span class="text follow"><em>Follow-Up Sha-</em> ring Sessions</span>
										</span>
										<span>
											<span class="img-holder">
												<img src="images/img4.png" width="48" height="45" alt="image description" />
											</span>
											<span class="text"><em>Sustainability</em> Webinars</span>
										</span>
									</a>
								</li>
							</ul>
							<h2>Communication Platforms</h2>
							<ul class="box-list">
								<li>
									<a href="#">
										<span class="box">
											<img src="images/img6.png" width="47" height="44" alt="image description" />
											<span>Success Stories</span>
										</span>
										<span class="box">
											<img src="images/img7.png" width="48" height="45" alt="image description" />
											<span>Your Portal</span>
										</span>
									</a>
								</li>
								<li>
									<a href="#">
										<span class="box">
											<img src="images/img8.png" width="44" height="45" alt="image description" />
											<span>Social Tools</span>
										</span>
										<span class="box">
											<img src="images/img7.png" width="48" height="45" alt="image description" />
											<span>Executive Sponsor<br/>Message</span>
										</span>
									</a>
								</li>
								<li>
									<a href="#">
										<span class="box">
											<img src="images/img10.png" width="43" height="47" alt="image description" />
											<span>“Eat Light, Eat Often”<br/>Mini Event</span>
										</span>
										<span class="box">
											<img src="images/img9.png" width="49" height="47" alt="image description" />
											<span>“Recovery”<br/> Mini Event</span>
										</span>
									</a>
								</li>
							</ul>
						</div>
						<div class="white-text">Our Implementation Experts will work with you to customize a rollout plan for your unique culture and strategic business objectives.</div>
					</div>
					
					<span class="opener-holder">
						<a href="#" class="opener"><span>E-Course Map</span></a>
					</span>
				</div>
			</div>
			<div id="footer">
				<div class="holder">
					<ul class="sub-nav">
						<li><a href="/about.php">About Us</a></li>
						<li><a href="/course-contents.php">Course Contents</a></li>
						<li><a href="/overview.php">Turnkey Implementation</a></li>
						<li><a href="http://www.hpinstitute.com/">hpinstitute.com</a></li>
						<li><a class="get-file" href="http://get.adobe.com/reader/">Get Adobe PDF Reader</a></li>
					</ul>
					<address>
						9757 Lake Nona Road Orlando, Florida 32827 <br />Phone: 407.438.9911 Fax: 407.438.6667
					</address>
					<ul class="social-networks">
						<li><a class="linkedin" href="http://www.linkedin.com/company/human-performance-institute">linkedin</a></li>
						<li><a class="facebook" href="http://www.facebook.com/HumanPerformanceInstitute">facebook</a></li>
						<li><a class="twitter" href="http://www.twitter.com/teamhpi">twitter</a></li>
						<li><a class="youtube" href="http://www.youtube.com/watch?v=Gxq7sQoWxyg&playnext=1&list=PL0BE45982D331DC20&feature=results_main">youtube</a></li>
					</ul>
					<p>This site is governed solely by applicable U.S. laws and governmental regulations. Please see our Privacy Policy. Use of this site constitutes your consent to application of such laws and regulations and to our Privacy Policy. Your use of the information on this site is subject to the terms of our Legal Notice. You should view the News section and the most recent SEC Filings in the Investor section in order to receive the most current information made available by Johnson &amp; Johnson Services, Inc. Contact Us with any questions or search this site for more information.</p>
					<span class="copyright">All contents Copyright &copy; 2009-2013 Human Performance Institute, Inc. All Rights Reserved.</span>
					<a href="#header" class="skip bottom">Back To Top</a>
				</div>
			</div>
		</div>
	</div>
</body>
</html>