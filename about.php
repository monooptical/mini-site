<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>About | HPI eLearning Course</title>
		<link rel="stylesheet" type="text/css" href="css/all.css" media="screen"/>
	</head>
	<body>
		<div id="wrapper">
			<div id="header-wrapper">
				<div id="header" class="w1">
					<strong class="logo"><a href="/">human performance institute</a></strong>
					<strong class="slogan">Energy for performance e-course</strong>
					<div class="contact">
						<p>Call <span>1-407-438-9911</span><br /> or <a href="mailto:info@hpinstitute.com">Contact Us</a> via Email</p>
					</div>
				</div>
			</div>
			<div id="main-wrapper">
				<div id="main" class="w1">
					<div class="banner-block inner">
						<div class="visual"><img src="images/img02.jpg" alt="image description" width="969" height="340" /></div>
					</div>
					<div class="section">
						<div class="content">
							<h1>About the Human Performance Institute</h1>
							<p><em>Helping leaders expand their energy and perform at their personal best.</em> That is what the Human Performance Institute has been a pioneer in studying since its co-founding by Drs. Jim Loehr and Jack Groppel. Built on more than 30 years of proprietary research and training with elite performers, the Institute’s team is comprised of expert performance coaches, exercise physiologists, and nutritionists that together have trained countless elite performers in high stress arenas, from Olympic medalists, professional athletes, CEOs, Hostage Rescue Teams, and military Special Forces.</p>
							<p>The pioneer in Energy Management Technology, the Human Performance Institute’s technology of managing energy is measurement-based and grounded in the sciences of performance psychology, exercise physiology, and nutrition. The Institute’s premier offering, the Corporate Athlete&reg; training is a 2 &frac12;-day intensive training program focused on expanding and managing individual energy capacity. The Corporate Athlete Course helps empower individuals to become more productive and effective under pressure, as well as achieve sustained high performance, both at work and at home.</p>
							<p>Headquartered in Orlando, Florida, the Human Performance Institute is situated on a 9-acre campus at Lake Nona in Orlando. Located only 10 minutes from the Orlando International Airport, the Institute’s campus consists of a conference center, comprehensive testing and diagnostic facilities, a state-of-the-art Fitness Center, and a world-class Tennis Center.</p>
							<p>The Human Performance Institute is a part of the Johnson &amp; Johnson Family of Companies and the Wellness and Prevention business. The Institute’s products and services underscore the global company’s commitment to promoting health and wellness as a way of reducing healthcare-related costs, improving overall employee health and increasing productivity.</p>
							<a href="/" class="back"><span>Back</span></a>
							<div class="info">Call <b class="mark">407-438-9911</b> to Request a Demo</div>
						</div>
					</div>
				</div>
			</div>
			<div id="footer-wrapper">
				<div id="footer" class="w1">
					<ul class="nav">
						<li><a href="/about.php">About Us</a></li>
						<li><a href="/course-contents.php">Course Contents</a></li>
						<li><a href="/overview.php">Turnkey Implementation</a></li>
						<li><a href="http://www.hpinstitute.com/">hpinstitute.com</a></li>
						<li><a class="get-file" href="http://get.adobe.com/reader/">Get Adobe PDF Reader</a></li>
					</ul>
					<div class="info">
						<address>9757 Lake Nona Road Orlando, Florida 32827</address>
						<dl>
							<dt>Phone:</dt>
							<dd>407.438.9911</dd>
							<dt>Fax:</dt>
							<dd>407.438.6667</dd>
						</dl>
					</div>
					<ul class="social-icon">
						<li><a href="http://www.linkedin.com/company/human-performance-institute" class="linkedin" target="_blank">linkedin</a></li>
						<li><a href="http://www.facebook.com/HumanPerformanceInstitute" class="facebook" target="_blank">facebook</a></li>
						<li><a href="http://www.twitter.com/teamhpi" class="twitter" target="_blank">twitter</a></li>
						<li><a href="http://www.youtube.com/watch?v=Gxq7sQoWxyg&playnext=1&list=PL0BE45982D331DC20&feature=results_main" class="youtube" target="_blank">youtube</a></li>
					</ul>
					<p>This site is governed solely by applicable U.S. laws and governmental regulations. Please see our Privacy Policy. Use of this site constitutes your consent to application of such laws and regulations and to our Privacy Policy. Your use of the information on this site is subject to the terms of our Legal Notice. You should view the News section and the most recent SEC Filings in the Investor section in order to receive the most current information made available by Johnson &amp; Johnson Services, Inc. Contact Us with any questions or search this site for more information.</p>
					<p class="copy">All contents Copyright &copy; 2009-2012 Human Performance Institute, Inc. All Rights Reserved.</p>
				</div>
			</div>
		</div>
	</body>
</html>
