<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>Course Contents | HPI eLearning Course</title>
		<link rel="stylesheet" type="text/css" href="css/all.css" media="screen"/>
	</head>
	<body>
		<div id="wrapper">
			<div id="header-wrapper">
				<div id="header" class="w1">
					<strong class="logo"><a href="/">human performance institute</a></strong>
					<strong class="slogan">Energy for performance e-course</strong>
					<div class="contact">
						<p>Call <span>1-407-438-9911</span><br /> or <a href="mailto:info@hpinstitute.com">Contact Us</a> via Email</p>
					</div>
				</div>
			</div>
			<div id="main-wrapper">
				<div id="main" class="w1">
					<div class="banner-block inner">
						<div class="visual"><img src="images/img02.jpg" alt="image description" width="969" height="340" /></div>
					</div>
					<div class="section">
						<div class="content">
							<h1>Course Contents</h1>
							<p>The ENERGY FOR PERFORMANCE&reg; e-Course includes 6 topics containing a total of 17 lessons with application exercises to further enhance the learning experience.  There is also an extensive “Explore Further” section with over 50 additional lessons providing a deeper dive into core energy management principles.</p>
							<p>The 6 topics include:</p>
							<h2>TOPIC 1</h2>
							<div class="lesson">
							<p><strong>Lesson 1: Energy for Performance&reg; in Life</strong></p>
							<p>Are there situations in your own life where you “show up” but actually miss an opportunity?  Learn why energy, not time, is our most precious resource.</p></div>
							<h2>TOPIC 2</h2>
							<div class="lesson">
							<p><strong>Lesson 2: Who or What Deserves Your Best Energy?</strong></p>
							<p>This lesson will help participants uncover their Ultimate Mission – the purpose that drives them.</p></div>
							<h2>TOPIC 3: How Can Energy Technology Help You?</h2>
							<div class="lesson">
							<p>This topic includes seven lessons covering seven key energy management technology principles.   Each lesson includes an assignment that will help the participant apply the principle in his/her life.</p>
							<p><strong>Lesson 3: Ignite Your Four Dimensions of Energy</strong></p>
							<p>To bring your full and best energy to the moments that matter, you need to be aware of how you are currently managing your energy in four separate, but interconnected dimensions.  Participants will learn about these dimensions, then take a few days to use the 4D energy audit provided to rate their energy levels.</p>
							<p><strong>Lesson 4: Balance Stress and Recovery</strong></p>
							<p>Expanding your energy capacity means using stress for growth.  How do you do that?  You start by identifying areas where you need more stress and where you need more recovery.  This lesson will help participants make a plan to build in more investments in stress and more recovery for growth.</p>
							<p><strong>Lesson 5:  Get Your Story Straight</strong></p>
							<p>The stories we tell ourselves play an important role in our ability to make changes in our lives.  In this lesson participants will learn about the types of common stories that tend to keep us from success and they’ll have the chance to identify a wrong story that might be keeping them from completing their mission.</p>
							<p><strong>Lesson 6:   Avoid Multi-tasking</strong></p>
							<p>Many people consider multi-tasking a valuable skill.  But when it comes to managing your energy, it may be impacting your ability to be fully present with the people and things that matter most.  In this lesson participants will be challenged to recognize how this is impacting their mission and identify where they should multi-task less.</p>
							<p><strong>Lesson 7:  Recognize Your Opportunistic Emotions</strong></p>
							<p>The quality of our energy is impacted by the quality of our emotions.  Participants will learn about opportunistic emotions, and then take a few days to track their emotions and identify which types they experience the most, which ones they may want to invest in for growth, and which ones they want to stop investing in.</p>
							<p><strong>Lesson 8:   Eat Light, Eat Often</strong></p>
							<p>Have you ever thought about using nutrition to manage your energy throughout the day?  In this lesson participants will be taught how to identify immediate actions to take to apply the “eat light, eat often” concept,  identify new routines that might be required to support the effort, create a plan for obstacles, and recognize if there is a story that could prevent them from applying this concept.</p>
							<p><strong>Lesson 9:   Move Strategically</strong>
							<p>Our bodies were designed to move, and strategically building movement into your day can help you boost your metabolism and keep you energized.  This lesson includes a strategic movement job aid to use to create a weekly plan for building strategic movement into the day.</p>
							</div>
							<h2>TOPIC 4: Who or What Is Getting Your Best Energy Now?</h2>
							<div class="lesson">
							<p>This topic includes six lessons covering methods for helping overcome barriers in each of the four dimensions.  Lessons 11-15 end with a ritual-building challenge exercise that will allow participants to try the approach out for themselves.</p>
							<p><strong>Lesson 10:   Assess Your Current Reality</strong></p>
							<p>This lesson will help participants evaluate areas in their life where they are experiencing misalignment.  It includes an assessment for them to identify the barriers they may be facing in each of the four dimensions.</p>
							<p><strong>Lesson 11:   Reconnect with Purpose</strong></p>
							<p>If you are feeling uncommitted and uninspired, you might need to reconnect with purpose.  This lesson will help participants do just that.</p>
							<p><strong>Lesson 12:  Use Mental Training Strategies</strong></p>
							<p>Participants with barriers in the mental dimension may benefit from this lesson which will help them learn ways to use mental training strategies to overcome their barriers.</p>
							<p><strong>Lesson 13: Grow Opportunistic Emotions</strong></p>
							<p>Participants with barriers in the emotional dimension can learn to choose to invest 60 seconds every morning writing down what they are grateful for.</p>
							<p><strong>Lesson 14:  Eat Strategically</strong></p>
							<p>For those experiencing barriers in the physical dimension, they can use the meal planning work sheet for one week to plan their meals and snacks.</p>
							<p><strong>Lesson 15:  Exercise Strategically</strong></p>
							<p>Another option for the physical dimension is to use exercise strategically.  In this lesson participants can use the workout plan to schedule aerobic, resistance and flexibility training for a week.</p></div>
							<h2>TOPIC 5</h2>
							<div class="lesson">
							<p><strong>Lesson 16: How Can You Invest Your Energy to Get Back on Track?</strong></p>
							<p>At this point, participants have had the chance to learn and practice all of the energy management concepts and are ready to create their personal plan for change.  This lesson will take participants through the 6-step change process including refining their Ultimate Mission, developing a Training Mission, writing their Old Story, creating a New Story, constructing supporting rituals, and creating an accountability and support system.</p></div>
							<h2>TOPIC 6</h2>
							<div class="lesson">
							<p><strong>Lesson 17:  Complete the Mission.</strong></p>
							<p>Consider how you define success in your life:  Where is failure not an option?  Participants will have the tools to complete their mission.  This final lesson is for inspiration.</p>
							<a href="/" class="back"><span>Back</span></a></div>
							<div class="info">Call <b class="mark">407-438-9911</b> to Request a Demo</div>
						</div>
					</div>
				</div>
			</div>
			<div id="footer-wrapper">
				<div id="footer" class="w1">
					<ul class="nav">
						<li><a href="/about.php">About Us</a></li>
						<li><a href="/course-contents.php">Course Contents</a></li>
						<li><a href="/overview.php">Turnkey Implementation</a></li>
						<li><a href="http://www.hpinstitute.com/">hpinstitute.com</a></li>
						<li><a class="get-file" href="http://get.adobe.com/reader/">Get Adobe PDF Reader</a></li>
					</ul>
					<div class="info">
						<address>9757 Lake Nona Road Orlando, Florida 32827</address>
						<dl>
							<dt>Phone:</dt>
							<dd>407.438.9911</dd>
							<dt>Fax:</dt>
							<dd>407.438.6667</dd>
						</dl>
					</div>
					<ul class="social-icon">
						<li><a href="http://www.linkedin.com/company/human-performance-institute" class="linkedin" target="_blank">linkedin</a></li>
						<li><a href="http://www.facebook.com/HumanPerformanceInstitute" class="facebook" target="_blank">facebook</a></li>
						<li><a href="http://www.twitter.com/teamhpi" class="twitter" target="_blank">twitter</a></li>
						<li><a href="http://www.youtube.com/watch?v=Gxq7sQoWxyg&playnext=1&list=PL0BE45982D331DC20&feature=results_main" class="youtube" target="_blank">youtube</a></li>
					</ul>
					<p>This site is governed solely by applicable U.S. laws and governmental regulations. Please see our Privacy Policy. Use of this site constitutes your consent to application of such laws and regulations and to our Privacy Policy. Your use of the information on this site is subject to the terms of our Legal Notice. You should view the News section and the most recent SEC Filings in the Investor section in order to receive the most current information made available by Johnson &amp; Johnson Services, Inc. Contact Us with any questions or search this site for more information.</p>
					<p class="copy">All contents Copyright &copy; 2009-2012 Human Performance Institute, Inc. All Rights Reserved.</p>
				</div>
			</div>
		</div>
	</body>
</html>
